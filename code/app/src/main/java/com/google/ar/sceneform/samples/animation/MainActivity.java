/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.ar.sceneform.samples.animation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;

import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.SkeletonNode;
import com.google.ar.sceneform.animation.ModelAnimator;

import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.AnimationData;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.samples.animation.speech.Action;
import com.google.ar.sceneform.samples.animation.speech.ISpeech;
import com.google.ar.sceneform.samples.animation.speech.Speech;
import com.google.ar.sceneform.ux.ArFragment;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Demonstrates playing animated FBX models.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "AnimationSample";
    private static final int ANDY_RENDERABLE = 1;
    private ArFragment arFragment;
    private ModelLoader modelLoader;
    private ModelRenderable andyRenderable;
    private AnchorNode anchorNode;
    private ModelAnimator animator;
    private FloatingActionButton animationButton;

    private SkeletonNode andy;

    private ISpeech speech;


    private static final int MY_PERMISSIONS = 1;
    private String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
    };


    private List<String> movingActions = new ArrayList<>(Arrays.asList("walk", "run"));

//    private float extentZ;
//    private float extentX;

    private static final float runPosFactor = 0.025f;
    private static final float walkPosFactor = 0.0075f;


    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PERMISSIONS);
        } else {
            initView();
            initSpeech();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void initSpeech() {
        speech = new Speech(this);
        speech.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Action action = speech.getRecognizedAction();
                onPlayAnimation(action.toString().toLowerCase());
            }
        });
        animationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speech.startListening();
            }
        });
    }

    public void initView() {
        setContentView(R.layout.activity_main);
        animationButton = findViewById(R.id.animate);
        disableButton();

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.sceneform_fragment);
        modelLoader = new ModelLoader(this);
        modelLoader.loadModel(ANDY_RENDERABLE, R.raw.andy);
        arFragment.setOnTapArPlaneListener(this::onPlaneTap);


        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onFrameUpdate);
    }


    private float counter = 0.0f;

    private void onFrameUpdate(FrameTime frameTime) {
        if (animator != null) {
            if (animator.isRunning()) {
                String currAction = animator.getName();
                if (movingActions.contains(currAction)){
                    float posFactor;
                  //  float scaleFactor;
                    if (currAction.equals("walk")) {
                        posFactor = walkPosFactor;
                      //  scaleFactor = 0.0025f;
                    } else {
                        posFactor = runPosFactor;
                        //scaleFactor = 2 * 0.0025f;
                    }

                    Vector3 currentPos = andy.getWorldPosition();
                    Vector3 currentScale = andy.getWorldScale();
                    switch (direction) {
                        case FORWARD:
                            andy.setWorldPosition(new Vector3(currentPos.x, currentPos.y, currentPos.z - posFactor));
                            //andy.setWorldScale(new Vector3(currentScale.x - scaleFactor, currentScale.y - scaleFactor, currentScale.z - scaleFactor));
                            break;
                        case BACKWARD:
                            andy.setWorldPosition(new Vector3(currentPos.x, currentPos.y, currentPos.z + posFactor));
                            //andy.setWorldScale(new Vector3(currentScale.x + scaleFactor, currentScale.y + scaleFactor, currentScale.z + scaleFactor));
                            break;
                    }
                    counter += posFactor;
                    if (counter > 0.75) {
                        counter = 0;
                        turn();
                    }
                    Log.d("velz", "" + (currentPos.z - posFactor));
                    Log.d("counter", "" + counter);
                }

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS: {

                if (hasAllPermissionsGranted(grantResults)) {
                    initView();
                    initSpeech();
                } else {
                    Toast toast = Toast.makeText(this, "Nisu sve privole prihvaćene, bye bye", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    finish();
                }
            }
            return;
        }

    }

    public boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }


    private void onPlayAnimation(String action) {
        if (action.equals("turn")) {
            turn();
            return;
        }
        if (animator != null) {
            if (animator.isRunning()) {
                animator.cancel();
            }
        }
        if (animator == null || !animator.isRunning()) {
            AnimationData data = andyRenderable.getAnimationData(action);
            animator = new ModelAnimator(data, andyRenderable);
            animator.setRepeatCount(ModelAnimator.INFINITE);
            animator.start();
        }
    }

    @SuppressLint("RestrictedApi")
    public void disableButton() {
        animationButton.setVisibility(View.GONE);
        animationButton.setEnabled(false);
    }

    @SuppressLint("RestrictedApi")
    public void enableButton() {
        animationButton.setVisibility(View.VISIBLE);
        animationButton.setEnabled(true);
    }

    /*
     * Used as the listener for setOnTapArPlaneListener.
     */
    private void onPlaneTap(HitResult hitResult, Plane unusedPlane, MotionEvent unusedMotionEvent) {
        if (andyRenderable == null) {
            return;
        }
        // Create the Anchor.

        Anchor anchor = hitResult.createAnchor();
        if (anchorNode == null) {
            anchorNode = new AnchorNode(anchor);
            anchorNode.setParent(arFragment.getArSceneView().getScene());


            //ove dimenzije iskoristiti za optimalnije kretanje po plane-u ... (kod countera i dodavanje)
//            extentX = unusedPlane.getExtentX();
//            extentZ = unusedPlane.getExtentZ();

//            Log.d("opsegz", "" + extentZ);

            andy = new SkeletonNode();
            andy.setParent(anchorNode);
            andy.setRenderable(andyRenderable);
            andy.setWorldScale(new Vector3(0.5f, 0.5f, 0.5f));
            direction = Direction.FORWARD;

            turn();

            enableButton();
            onPlayAnimation("idle");
        }


    }

    private Direction direction;

    private void turn() {
        switch (direction) {
            case FORWARD:
                andy.setLocalRotation(Quaternion.axisAngle(new Vector3(0, 1f, 0), 180));
                direction = Direction.BACKWARD;
                break;
            case BACKWARD:
                andy.setLocalRotation(Quaternion.axisAngle(new Vector3(0, 1f, 0), 0));
                direction = Direction.FORWARD;
                break;
        }
        if (counter > 0) {
            counter = 0.75f - counter;
        }

    }


    void setRenderable(int id, ModelRenderable renderable) {
        if (id == ANDY_RENDERABLE) {
            this.andyRenderable = renderable;
        }
    }

    void onException(int id, Throwable throwable) {
        Toast toast = Toast.makeText(this, "Unable to load renderable: " + id, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.e(TAG, "Unable to load andy renderable", throwable);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null && modelLoader != null) {
            speech.stopListening();
            anchorNode = null;
            if (animator != null) {
                animator.end();
            }
            animator = null;
            andyRenderable = null;
            andy = null;
            disableButton();
            counter = 0;
            modelLoader.loadModel(ANDY_RENDERABLE, R.raw.andy);
        }
    }
}