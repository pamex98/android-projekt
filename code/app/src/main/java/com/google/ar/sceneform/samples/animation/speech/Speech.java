package com.google.ar.sceneform.samples.animation.speech;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.google.ar.sceneform.samples.animation.speech.Action;
import com.google.ar.sceneform.samples.animation.speech.ISpeech;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class Speech implements ISpeech, RecognitionListener {

    private PropertyChangeSupport changes = new PropertyChangeSupport(this);

    private Map<Action, List<String>> actions = new HashMap<>();
    private SpeechRecognizer speechRecog;
    private Context mainActivity;
    private Action nextAction = null;

    public Speech(Context context) {
        mainActivity = context;
        init();
        mapActions();
    }


    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }

    // iz filea- dodati
//    private void mapActions() {
//        ArrayList<String> walk = new ArrayList<>();
//        ArrayList<String> jump = new ArrayList<>();
//        ArrayList<String> dance = new ArrayList<>();
//        walk.add("hodaj");
//        jump.add("skoči");
//        dance.add("pleši");
//        actions.put(Action.WALK,walk );
//
//        actions.put(Action.DANCE, dance);
//        actions.put(Action.JUMP, jump);
//    }

    private void mapActions() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(mainActivity.getAssets().open("sinonimi.txt"), "UTF-8"));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                Log.d("radi", mLine);
                String action = mLine.split(":")[0];
                String[] sinonimi = mLine.split(":")[1].split(",");
                try {
                    Action key = Action.valueOf(action);
                    List<String> values = Arrays.asList(sinonimi);
                    actions.put(key, values);
                } catch (IllegalArgumentException a) {
                    Log.d("error", action + " nije podržana akcija");
                }
            }
        } catch (IOException e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }
    }


    @Override
    public void startListening() {
        Log.d("provjera", "startListening: ");
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "hr-HR");
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        speechRecog.startListening(intent);
    }

    @Override
    public void stopListening() {
        speechRecog.stopListening();
        Log.d("provjera", "stopListening: ");
    }

    @Override
    public Action getRecognizedAction() {
        return nextAction;
    }


    public void init() {
        if (SpeechRecognizer.isRecognitionAvailable(mainActivity)) {
            speechRecog = SpeechRecognizer.createSpeechRecognizer(mainActivity);
            speechRecog.setRecognitionListener(this);
        }
    }

    @Override
    public void onReadyForSpeech(Bundle bundle) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float v) {

    }

    @Override
    public void onBufferReceived(byte[] bytes) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int i) {

    }

    @Override
    public void onResults(Bundle bundle) {
        List<String> result_arr = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        processResult(result_arr.get(0));
    }

    private void processResult(String s) {
        for (Map.Entry<Action, List<String>> entry : actions.entrySet()) {
            for (String val : entry.getValue()) {
                if (val.equals(s)) {
                    Action action = entry.getKey();
                    changes.firePropertyChange("nextAction", null, nextAction = action);
                    return;
                }
            }
        }

    }

    @Override
    public void onPartialResults(Bundle bundle) {

    }

    @Override
    public void onEvent(int i, Bundle bundle) {

    }
}
