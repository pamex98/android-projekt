package com.google.ar.sceneform.samples.animation.speech;

import com.google.ar.sceneform.samples.animation.speech.Action;

import java.beans.PropertyChangeListener;

public interface ISpeech {

    void startListening();

    void stopListening();

    Action getRecognizedAction();

    void addPropertyChangeListener(PropertyChangeListener l);

    void removePropertyChangeListener(PropertyChangeListener l);

}
