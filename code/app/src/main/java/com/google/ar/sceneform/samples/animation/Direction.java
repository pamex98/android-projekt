package com.google.ar.sceneform.samples.animation;

public enum Direction {

    FORWARD, BACKWARD
}
